# Vladimir's README (Manager, Solutions Architects, Commercial, EMEA)

This page provides a bit of information about me and my work style so others can get an idea of what it might be like to work with me. 

## A little about me 

- I am originally from a small town in Eastern Kazakhstan where I spent my first 15 years before relocating to Russia (where my family is originally from). 
- I received my education in Russia, Czech Republic and Italy and consider these to be very formative years that prepared me for true cross-cultural collaboration. 
- In April 2008 I moved to Amsterdam, The Netherlands and consider this to be my homebase ever since. 
- Before joining I GitLab in 2019, I spent 10+ years working as a software developer, consultant, solutions architect and an engineering manager in RFID (Radio Frequency Identification) market. These years have formed my passion for bridging business and IT worlds, and making sure technology is used to deliver tangible business value. Find more about my previous career at https://dzalbo.com/ 
- Apart from DevOps, Cloud Computing and Open Source, I am very passionate about all things Urban Design and Smart Cities. 
- I love to travel and I travel a lot. I’ve been to [77 UN+ countries](https://nomadmania.travel/profile/34799) so far. 

## My working style 

- **Having structure and adhering to plan** is my way of getting things done. I prepare a bullet list of activities than need to be done at the beginning of every week and adjust it daily to add support for flexibility. Once things get scheduled, I place utmost importance on respecting the deadlines and timings. 
- **Calendar**: I do my best to keep my calendar up to date. That includes focus and preparation time. If you are planning to schedule a meeting that coincides with a time slot marked as "preparation” or “focus time”, please check with me in advance before confirming dates and times with the customer. This will ensure we do minimize rescheduling and at the last moment. 
- **Agenda**: I prefer to have an agenda prepped and ready to go for any meeting, with the exception of coffee chats. Agenda can be shared as an attached document or a bullet list in the invitation. When there is no agenda, it translates to you not caring or not being respectful of other’s time. 
- **Response time**: I will always do my best to provide same-day responses to both internal or external communication, even if only to acknowledge that the message had been received. I will either use this message to provide detailed feedback or to set expectations for when it can be expected. If you feel that my feedback is being delayed, I encourage you to send me a reminder. Slack is the easiest way to find me most of the time. 
- **Asynchronous communication**: I have a strong preference for asynchronous communication and will default to it when possible. 
- **Synchronous communication**: While I tend to default to async communication as a starting point, I also recognize the importance of synchronous communication in terms of building rapport, trust, and quickly delivering shared context to a group. This is particularly valid for any engagements with customers or third parties. 
- **Where information is stored**: You can assume that all information is kept in the Customers & Prospects folder in Google Drive under the appropriately designated letter / customer name. All customer meetings and email communication will also be stored as a Salesforce Activity. If you prefer to record notes and actions differently, please let me know what that collaborative method is. 

## Culture Map 

In order to provide better insights into my working style, preferences, biases and cultural tendencies I have also performed a self-assessment across 8 cultural dimensions. This assessment is based on Erin Meyer's book [The Culture Map](https://erinmeyer.com/books/the-culture-map/). Some of these can be seen as my strengths or my weaknesses. I am sharing these details hoping to build better trust, especially with those who haven't worked with me before. 

### Communicating 

I prefer low-context communication style and consider good communication to be precise, simple, and clear. I always strive to apply effort in crafting my messages in such a way that they do not require reading between lines and can be understood at face value. My language might miss eloquence sometimes, but I consider language to be a tool for delivering information. This (among other things) means that I both use repetition and appreciate when it is used by others to stress importance and to clarify a message. 

I strive to ensure that communicating with me requires little to no context. It should not matter if we know each other for years or have just recently started working together. 

I strongly believe in the [Simple Language](https://about.gitlab.com/handbook/communication/#sts=Simple%20Language) principle of using the most clear, straightforward, and meaningful words possible in every conversation. I avoid using "fluff" words, jargon, or "corporate-speak" phrases that don't add value. 

All said above counts for both asynchronous (emails, messages, documentation, Slack, issues) and synchronous (calls, meetings) communication. However, I have a strong preference towards asynchronous channels at least as a starting point. 

### Evaluating 

While I understand and can appreciate the value of direct feedback, I am usually more inclined towards indirect negative feedback towards others. If I do give one, I will most probably deliver it softly, subtly and somewhat diplomatically. I believe it is important to accompany any negative feedback with a positive message. 

It is very probable that I will make frequent use of downgraders and qualifying descriptors in my speech (_kind of, sort of, a little bit, maybe, and slightly_). I will only give negative feedback in the smallest setting possible (preferably 1:1).  

This approach might seem controversial given my preference for low-context communication, but I strongly believe that these are two separate cultural scales. 

Having said this, I do not require the same approach towards myself. I welcome frank, blunt and honest communication from those culturally more inclined towards direct negative feedback. When on receiving side, I will strive to [assume positive intent](https://about.gitlab.com/handbook/values/#sts=Assume%20positive%20intent).

### Persuading 

Through my education I have been mainly trained to apply principles-first, deductive reasoning for persuasion deriving conclusions and facts from general principles or concept. I have a naturally formed distrust of application-first thinking that tends to focus on statements or opinions without being properly backed by theoretical knowledge. 

In practice this means that I would in some cases require additional time to consolidate my thinking and to develop a theory or a concept before I can commit to presenting my ideas, statements or opinions. I would normally try begin my message or report by building up a theoretical argument before moving on to a conclusion. I highly value conceptual principles underlying any specific situation. 

In business or personal negotiations, I frequently follow Hegelian dialectic method of presenting a thesis, antithesis and synthesis. 

### Leading 

Having spent most of my professional life in egalitarian societies, I am a strong proponent of egalitarian rather than hierarchical leadership models. In order to thrive at my job, I find it important to maintain very low distance between me and my manager, between me and my subordinates. 

A good manager in my book is someone who empowers their reports to make decisions and own them through providing comfortable environment and asking the right questions rather than commanding. 

I prefer working in organizations with flat structures and low power distances that allow skipping hierarchical lines for effective communication. 

I believe that it is OK: 
- To disagree with your boss even in front of other teammates 
- To demonstrate bias for action even without explicit manager’s approval: decisions should be thoughtful, but delivering fast results requires the fearless acceptance of occasionally making mistakes 
- To cross hierarchical boundaries and skip levels 
- To go directly to the source when communicating with someone outside your team (no need to ask managers to establish a communication channel in advance) 

### Deciding

While I strongly believe in consensual decision-making process through unanimous agreement, I also recognize that consensual organizations lack speed, leading to projects happening under the radar. At the same time hierarchical organizations have good speed but are bad at gathering data, leading to people saying yes but not doing it. 

My general approach to decision-making usually follows a number of rules: 
1. Default to a consensus-based process when the decision that needs to be made relates only to my direct team (hence the risk of paralysis by analysis is small). 
1. For decisions impacting larger parts of the organization, [combine the best of two worlds by splitting decisions into two phases](https://about.gitlab.com/company/culture/all-remote/management/#separating-decision-gathering-from-decision-making). The data gathering phase has the best of consensus organizations, where everyone can contribute. The decision phase has the best of a hierarchical organization, the person that does the work or their manager decides what to do. 
1. Document and communicate the decision in precise, simple and clear way that includes following elements 
    - **Options** considered: what alternative approaches were considered, including the one that you recommend 
    - **Recommendation**: justification for why your preferred path was recommended over others. When appropriate and possible, this should include a recommendation grounded in data 
    - **Why is this smallest and fastest**: a statement on how you are taking an iterative approach. 

### Trusting

Historically I come from a culture that puts stronger value on [affective rather than cognitive trust](https://knowledge.insead.edu/blog/insead-blog/building-trust-across-cultures-3844), which usually arises from feelings of emotional closeness, empathy or friendship. I grew up in a relationship-based environment where trust is built through sharing meals, evening drinks, and encounters at the coffee machine. This means that I tend to build work relationships slowly over the long term. I need to see who my counterparts are at a deep level. I need to share personal time with them. I consider myself to be a good representative of “[coconut culture](https://hbr.org/2014/05/one-reason-cross-cultural-small-talk-is-so-tricky)”:  

> In coconut cultures such Russia and Germany, people are initially more closed off from those they don’t have friendships with. They rarely smile at strangers, ask casual acquaintances personal questions, or offer personal information to those they don’t know intimately. But over time, as coconuts get to know you, they become gradually warmer and friendlier. And while relationships are built up slowly, they also tend to last longer. 

I recognize however that all-remote, asynchronous workplace leaves limited opportunities for in-person relationship-building. As a result, I am currently focusing on cultivating and internalizing cognitive, task-based trust practices. I believe that better communication, collaboration and frequent iterations support me in this transition. 

I enjoy working with and trust those, who do their work consistently and reliably. I recognize that trust is a cornerstone of how any successful organization operates. I strongly believe that trust increases results, efficiency, and collaboration. I am very supportive and a frequent user of such trust-building tools as 1:1 meetings, social calls, offsite events, team days, etc. 

### Disagreeing 

I put high value on positive disagreement and dissent. I strongly believe that healthy debates are positive for teams in terms of building trust and achieving results. Open confrontation is appropriate and generally should not negatively impact relationships.  

I consider situations when everyone on the team falls in line without any debate, comments or disagreement to be indicative of some (or all) [five dysfunctions](https://about.gitlab.com/handbook/values/#five-dysfunctions): 

1. Absence of trust 
1. Fear of conflict 
1. Lack of commitment 
1. Avoidance of accountability 
1. Inattention to results 

As a result, I always advocate for constructive and passionate debate and encourage everybody to have “[short toes](https://about.gitlab.com/handbook/values/#short-toes)” (feeling comfortable letting others “step on our toes” by contributing to our domain). 

I make a separation however between confrontation and emotional expressiveness. While it is important to contribute directly and clearly, we should strive to keep disagreements healthy, constructive and avoid personal attacks or excessive emotional responses. These tend to have negative effect on team’s effectiveness, health and trust.  

### Scheduling / Concept of time

My concept of time tends to be much more linear than flexible. Characteristics of linear time include being punctual, following a set agenda, and starting and ending as scheduled. I place a lot of focus on the deadline and sticking to the schedule. I try my best to [be respectful of other’s time](https://about.gitlab.com/handbook/values/#be-respectful-of-others-time) (and I ask the same from others).  

I try to avoid unnecessary meetings, limit attendance to as few people as possible. All meeting should start and end on previously agreed time. If allocated time ends up being insufficient, I prefer to reschedule and use this as a lesson for better scheduling in the future. 

I consider most forms of interruption to be disruptive. I put a significant emphasis on promptness and good organization over unnecessary flexibility. 

## My career goals 

- I am ambitions and I aspire to see my career advance on a management track, as I believe this is the right way for me to have larger impact and amplify the results my team can achieve. 
- Reuse my previous management expertise (as an engineering manager) and combine with new knowledge of the company, the business and fully asynchronous workspace to build confidence to perform and meet expectations in a leadership role. 
- Develop my strengths, compensate for my weaknesses and become more self-aware in order to restart my career on the leadership path. 
- Connect to peers, superiors within GitLab as well external thought leaders to support my continuous learning and continuous improvement efforts. 
